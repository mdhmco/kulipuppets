# kulispuppets

Kugelschreiber Puppets

## Änderungen V7.14

- Hinweise auf Encoding in HTML Ausgabe

## Änderungen V7.13

- Entsorgung einer unnötigen Variablen "c" in Action "marathon_init"

## Änderungen V7.12

- Mehr Textuelle Hinweise im Puppetsource
- Überarbeitung manuelle Ergebniseingabe (CORRMODE-Variable)
- Löschung Actions "...kmp7m999"

## Änderungen V7.10

- Erste veröffentlichte Version
